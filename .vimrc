set nocompatible              " be iMproved, required
filetype off                  " required

let mapleader=","
"let mapleader = "\<Space>"
"let mapleader = " "
"let maplocalleader = "\<Space>"

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"Plugin 'nvie/vim-flake8'
Plugin 'bling/vim-airline'
"Plugin 'terryma/vim-multiple-cursors'
Plugin 'wincent/terminus' 
Plugin 'scrooloose/nerdcommenter' 
"Plugin 'scrooloose/nerdtree'

" Colour Schemes
Plugin 'chriskempson/base16-vim'
Plugin 'archseer/colibri.vim'

"Plugin 'ctrlpvim/ctrlp.vim'
"Plugin 'tacahiroy/ctrlp-funky'
"
"You complete me has dependencies
"Plugin 'Valloric/YouCompleteMe' 
Plugin 'ervandew/supertab'

"Plugin 'veloce/vim-behat'
"Plugin 'janko-m/vim-test'
"
Plugin '907th/vim-auto-save'

" Allows pasting with indents
Bundle 'sickill/vim-pasta'
Plugin 'junegunn/vim-easy-align'
Plugin 'Konfekt/FastFold'

"Python Plugins
Plugin 'tmhedberg/SimpylFold'
"Plugin 'ambv/black'
"Plugin 'vim-scripts/indentpython.vim'

"Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-unimpaired'
Plugin 'tpope/vim-surround' 
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-abolish'
"Bundle 'geoffharcourt/vim-matchit'
Plugin 'jiangmiao/auto-pairs'
Plugin 'qpkorr/vim-bufkill'
"Plugin 'w0rp/ale'
"Plugin 'pedrohdz/vim-yaml-folds'
"Plugin 'digitalrounin/vim-yaml-folds'
Plugin 'airblade/vim-gitgutter'
Plugin 'michaeljsmith/vim-indent-object'
Plugin 'roxma/vim-tmux-clipboard'
" Use leader j
Plugin 'pechorin/any-jump.vim'
Plugin 'majutsushi/tagbar'
nmap <F8> :TagbarToggle<CR>

" Add the fzf.vim plugin to wrap fzf:
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

:syntax enable

set backspace=indent,eol,start  " allow backspacing over everything in insert mode
set autoread                    " update a file that has changed in the background
set history=50                  " keep 50 lines of command line history
set ruler                       " show the cursor position all the time
set showcmd                     " display incomplete commands
set incsearch                   " do incremental searching
set nu                          " show line numbers
set expandtab                   " use spaces instead of tabs
set tabstop=4                   " insert 4 spaces whenever the tab key is pressed
set shiftwidth=4                " set indentation to 4 spaces
set hlsearch                    " highlight search terms
set ignorecase                  " Ignore Case during searches
set autoindent                  " start new line at the same indentation level
syntax enable                   " syntax highlighting
set cmdheight=1                 " The commandbar height
set showmatch                   " Show matching bracets when text indicator is over them
set nobackup                    " do not keep backup files, it's 70's style cluttering
set hidden                      " allow for unsaved buffers to exist
set ttyfast                     " speed up scrolling in  Vim

set noswapfile                  " do not write annoying intermediate swap files,
                                " who did ever restore from swap files
                                " anyway?
                                " https://github.com/nvie/vimrc/blob/master/vimrc#L141

set ttimeoutlen=50              " Solves: there is a pause when leaving insert mode
set splitbelow                  " Horizontal splits open below current file
set splitright                  " Vertical splits open to the right of the current file

set wildmode=longest,list,full  " Pressing <Tab> shows command suggestions similar to pressing <Tab>
set wildmenu                    " in bash 

set autowrite                   " Automatically :write before running commands

" Setup AutoSave
let g:auto_save = 1  " enable AutoSave on Vim startup

let g:AutoPairsMapSpace = 0
let g:AutoPairsShortcutToggle = "<Leader>p"
let g:AutoPairsShortcutFastWrap = "<Leader>w"
let g:AutoPairsShortcutJump = "<Leader>e"

" Make it obvious where 81 characters is
"set textwidth=80
"set colorcolumn=+1
"augroup vimrc_autocmds
    "autocmd BufEnter * highlight OverLength ctermbg=lightgrey guibg=#592929
    "autocmd BufEnter * match OverLength /\%80v.*/
"augroup END
"let &colorcolumn="80,".join(range(120,999),",")
let &colorcolumn="80"

let g:fastfold_force = 1

" option if terminal does not support 256 colours
"highlight ColorColumn ctermbg=8

" Map CTRL-P to FZF
nnoremap <C-p> :<C-u>FZF<CR> 

" fuzzy search in buffers
nmap <Leader>b :CtrlPBuffer<CR>

" fuzzy search in tags
nmap <Leader>f :CtrlPTag<CR>
" ctags -R --fields=+l --languages=python --python-kinds=-iv -f .tags ./
" set tag=.tags
" https://andrew.stwrt.ca/posts/vim-ctags/
 
" Easy expansion of the active file directory
cnoremap <expr> %%  getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" Shortcut to Mute Highlighting
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

" Vim Easy-Align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"Root permission on a file inside VIM
cmap w!! w !sudo tee >/dev/null %

" Airline stuff, can't live without it
" Populate powerline fonts
let g:airline_powerline_fonts = 1
set laststatus=2

"set guifont=Inconsolata\ for\ Powerline:h15
let g:Powerline_symbols = 'fancy'
set encoding=utf-8
set t_Co=256
set fillchars+=stl:\ ,stlnc:\
set term=xterm-256color
set termencoding=utf-8

" Auto open quickfix
augroup myvimrc
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l*    lwindow
augroup END

set clipboard=unnamed
" In visual mode, use Y to copy to system clipboard
vnoremap Y "*y
" In normal mode, do the same with the current line
nnoremap Y "*yy

"NerdTrees Stuff
"autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"nnoremap <Leader>f :NERDTreeToggle<Enter>

"ctrlp funky
let g:ctrlp_funky_multi_buffers = 1
nnoremap <Leader>fu :CtrlPFunky<Cr>

" Initialise list by a word under cursor
nnoremap <Leader>Fu :execute 'CtrlPFunky ' . expand('<cword>')<Cr>

"quick tab navigation
nnoremap <c-h> gT
nnoremap <c-l> gt

"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>

" Allow us to use Ctrl-s and Ctrl-q as keybinds
silent !stty -ixon
" Restore default behaviour when leaving Vim.
autocmd VimLeave * silent !stty ixon

"set save mappings
":nmap <c-s> :w<CR>
":vmap <c-s> <Esc><c-s>gv
":imap <c-s> <Esc><c-s>

" Get off my lawn
nnoremap <Left> :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>

"Vim Behat Configuration
"mandatory if you want the '*.feature' files to be set with behat filetype
let g:feature_filetype='behat'

" The plugin tries successively several behat executables to find the good one
" (php behat.phar, bin/behat, etc). You can define a custom list that will
" be prepended to the default path with g:behat_executables.
let g:behat_executables = ['behat.sh']

" if you use neocomplcache add this to enable behat completion
if !exists('g:neocomplcache_omni_patterns')
    let g:neocomplcache_omni_patterns = {}
endif
let g:neocomplcache_omni_patterns.behat = '\(When\|Then\|Given\|And\)\s.*$'

" disable omni completion steps cache
" normally you don't want to do this because it's slow (and will prevent neocomplcache from working)
" let g:behat_disable_omnicompl_cache = 1

" Deal with XML formatting using xmllint
au FileType xml setlocal equalprg=xmllint\ --format\ --recover\ -\ 2>/dev/null

" Handle yaml files
"autocmd FileType yaml echo "yaml - TestTestTest"
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" for command mode
nnoremap <S-Tab> <<
" for insert mode
inoremap <S-Tab> <C-d>
" jump to end of line while in insert mode
inoremap <C-e> <C-o>$

" Refer to the base16 color stuff - will automatically set vim to the 
" base-16 color scheme used
if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

:set mouse=a

" Vim Test Releated Mappings
nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

" The Silver Searcher
if executable('ag')
    " Use ag over grep
    set grepprg=ag\ --nogroup\ --nocolor

    " Use ag in CtrlP for listing files. Lightning fast and respects
    " .gitignore
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

    " ag is fast enough that CtrlP doesn't need to cache
    let g:ctrlp_use_caching = 0
endif

" bind K to grep word under cursor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" file is large from 10mb
let g:LargeFile = 1024 * 1024 * 10
augroup LargeFile 
	autocmd BufReadPre * let f=getfsize(expand("<afile>")) | if f > g:LargeFile || f == -2 | call LargeFile() | endif
augroup END

function! LargeFile()
	" no syntax highlighting etc
	set eventignore+=FileType
	" save memory when other file is viewed
	setlocal bufhidden=unload
	" is read-only (write with :w new_filename)
	setlocal buftype=nowrite
	" no undo possible
	setlocal undolevels=-1
	" display message
	autocmd VimEnter *  echo "The file is larger than " . (g:LargeFile / 1024 / 1024) . " MB, so some options are changed (see .vimrc for details)."
endfunction

"set t_Co=256
"colorscheme base16-monokai
":imap jj <Esc>

"Better window navigation
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Show list of open buffers
nnoremap gb :ls<CR>:b<Space>

" Select pasted text similar to built in gv
nnoremap <expr> gp '`[' . strpart(getregtype(), 0, 1) . '`]'

" Ale configurations - http://lee-w.github.io/posts/python/2017/03/tools-for-checking-python-coding-style/
" The default value is pytlinh
" If your pylint executable is not pylint, it should be set 
let g:ale_python_pylint_executable = 'pylint'

" options of your pylint command
let g:ale_python_pylint_options = '--rcfile ~/.pylintrc'

" Autosave folds
augroup AutoSaveFolds
  autocmd!
  autocmd BufWinLeave * if expand("%") != "" | mkview | endif | filetype detect
  autocmd BufWinEnter * if expand("%") != "" | loadview | endif | filetype detect
augroup END

" # Function to permanently delete views created by 'mkview'
function! MyDeleteView()
    let path = fnamemodify(bufname('%'),':p')
    " vim's odd =~ escaping for /
    let path = substitute(path, '=', '==', 'g')
    if empty($HOME)
    else
        let path = substitute(path, '^'.$HOME, '\~', '')
    endif
    let path = substitute(path, '/', '=+', 'g') . '='
    " view directory
    let path = &viewdir.'/'.path
    call delete(path)
    echo "Deleted: ".path
endfunction

" # Command Delview (and it's abbreviation 'delview')
"command Delview call MyDeleteView()
" Lower-case user commands: http://vim.wikia.com/wiki/Replace_a_builtin_command_using_cabbrev
"cabbrev delview <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'Delview' : 'delview')<CR>

nmap =j :%!python -m json.tool<CR>

au FileType xml setlocal equalprg=xmllint\ --format\ --recover\ -\ 3>/dev/null
au FileType json setlocal equalprg=python\ -m\ json.tool 3>/dev/null

func! GetSelectedText()
    normal gv"xy
    let result = getreg("x")
    return result
endfunc

" save to clipboard in windows
" https://stackoverflow.com/questions/44480829/how-to-copy-to-clipboard-in-vim-of-bash-on-windows
if !has("clipboard") && executable("clip.exe")
    noremap <C-C> :call system('clip.exe', GetSelectedText())<CR>
    noremap <C-X> :call system('clip.exe', GetSelectedText())<CR>gvx
endif

" F3: Toggle list (display unprintable characters).
nnoremap <F3> :set list!<CR>
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.


" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>


" XML formatter
function! DoFormatXML() range
	" Save the file type
	let l:origft = &ft

	" Clean the file type
	set ft=

	" Add fake initial tag (so we can process multiple top-level elements)
	exe ":let l:beforeFirstLine=" . a:firstline . "-1"
	if l:beforeFirstLine < 0
		let l:beforeFirstLine=0
	endif
	exe a:lastline . "put ='</PrettyXML>'"
	exe l:beforeFirstLine . "put ='<PrettyXML>'"
	exe ":let l:newLastLine=" . a:lastline . "+2"
	if l:newLastLine > line('$')
		let l:newLastLine=line('$')
	endif

	" Remove XML header
	exe ":" . a:firstline . "," . a:lastline . "s/<\?xml\\_.*\?>\\_s*//e"

	" Recalculate last line of the edited code
	let l:newLastLine=search('</PrettyXML>')

	" Execute external formatter
	exe ":silent " . a:firstline . "," . l:newLastLine . "!xmllint --noblanks --format --recover -"

	" Recalculate first and last lines of the edited code
	let l:newFirstLine=search('<PrettyXML>')
	let l:newLastLine=search('</PrettyXML>')
	
	" Get inner range
	let l:innerFirstLine=l:newFirstLine+1
	let l:innerLastLine=l:newLastLine-1

	" Remove extra unnecessary indentation
	exe ":silent " . l:innerFirstLine . "," . l:innerLastLine "s/^  //e"

	" Remove fake tag
	exe l:newLastLine . "d"
	exe l:newFirstLine . "d"

	" Put the cursor at the first line of the edited code
	exe ":" . l:newFirstLine

	" Restore the file type
	exe "set ft=" . l:origft
endfunction
command! -range=% FormatXML <line1>,<line2>call DoFormatXML()

nmap <silent> <leader>x :%FormatXML<CR>
vmap <silent> <leader>x :FormatXML<CR>

" # Function to permanently delete views created by 'mkview'
function! MyDeleteView()
    let path = fnamemodify(bufname('%'),':p')
    " vim's odd =~ escaping for /
    let path = substitute(path, '=', '==', 'g')
    if empty($HOME)
    else
        let path = substitute(path, '^'.$HOME, '\~', '')
    endif
    let path = substitute(path, '/', '=+', 'g') . '='
    " view directory
    let path = &viewdir.'/'.path
    call delete(path)
    echo "Deleted: ".path
endfunction

" # Command Delview (and it's abbreviation 'delview')
command Delview call MyDeleteView()
" Lower-case user commands: http://vim.wikia.com/wiki/Replace_a_builtin_command_using_cabbrev
cabbrev delview <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'Delview' : 'delview')<CR>
